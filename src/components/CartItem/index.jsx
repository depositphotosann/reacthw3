import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';
import { useImmer } from 'use-immer';

function CartItem({ 
  item = {},
  decrementCartItem = () => {},
  incrementCartItem = () => {},
  handleDelete = () => {},
  handleOpenModal
 }) {

  const totalPrice = item.price * item.count;

  const [isModalOpen, setIsModalOpen] = useImmer(false);

  const handleDeleteCartItem = () => {
    handleOpenModal(item, "cart");
    setIsModalOpen(true);
  };

  const handleConfirmDelete = () => {
    handleDelete();
    setIsModalOpen(false);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={styles.item}>
      <img src={item.image} alt={item.name} />
      <h2 className={styles.productName}>{item.name}</h2>
      <p className={styles.productPrice}>Price: {item.price}</p>
      <p className={styles.count}>Count: {item.count}</p>
      <p className={styles.count}>Total: {totalPrice} грн</p>
      <button disabled={item.count <= 1} onClick={() => {decrementCartItem(item.id)}}>-</button>
      <button onClick={() => {incrementCartItem(item.id)}}>+</button>
      <button onClick={handleDeleteCartItem}>Delete</button>

      {isModalOpen && (
        <div className={styles.modal}>
          <span className={styles.close} onClick={handleCloseModal}>&times;</span>
          <p>Ви впевнені, що хочете видалити цей товар з корзини?</p>
        
          <div className={styles.buttons}>
            <button onClick={handleCloseModal}>Скасувати</button>
            <button onClick={handleConfirmDelete}>Видалити</button>
          </div>
        </div>
      )}
    </div>
  );
}

CartItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.number,
    count: PropTypes.number,
  }),
  addToCart: PropTypes.func,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
};

export default CartItem;