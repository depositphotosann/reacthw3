import { NavLink } from 'react-router-dom';
import styles from './Header.module.scss';
import { useLocation } from 'react-router-dom';
import CartIcon from '../svg/CartIcon';
import { FaStar } from 'react-icons/fa';

const Header = ({ cartTotalCount = 0, favoritesTotalCount = 0 }) => {
  const location = useLocation();

  return (
    <header className={styles.header} >
      <span className={styles.logo}>Shopping coins</span>
      <nav>
        <ul>
          <li>
            <NavLink end to="/" className={({ isActive }) => isActive && styles.active}>Головна</NavLink>
          </li>

          <li>
            <NavLink end to="/cart" className={({ isActive }) => isActive && styles.active}>
              <div className={styles.cartContainer}>
                <CartIcon />
                <span>{cartTotalCount}</span>
              </div>
            </NavLink>
          </li>

          <li>
          <NavLink to="/favorites" className={({ isActive }) => isActive && styles.active}>
              <div className={styles.favoritesContainer}>
                <FaStar />
                <span>{favoritesTotalCount}</span>
              </div>
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;