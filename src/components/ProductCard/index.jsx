import PropTypes from "prop-types";
import styles from "./ProductCard.module.scss";
import { FaStar } from "react-icons/fa";
import { useImmer } from "use-immer";
import { useEffect, useState } from "react";

function ProductCard({
  product = {},
  addToCart = () => {},
  addToFavorites = () => {},
  favorites,
  addItemToFavorite,
  deleteItemFromFavorite,
}) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const { id, name, price, image, article } = product;

  const isInFavorite = favorites.some((favorite) => favorite.id === id);

  const [isFavorite, setIsFavorite] = useImmer(isInFavorite);

  // useEffect(() => {
  //   // const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
  //   setIsFavorite();
  // }, [id]);

  const handleToggleFavorites = (product) => {
    if (isInFavorite) {
      deleteItemFromFavorite(id);
      setIsFavorite((f) => !f);
    } else {
      addItemToFavorite(product);
      setIsFavorite((f) => !f);
    }
  };

  const handleAddToCart = () => {
    if (selectedProduct) {
      addToCart(selectedProduct);
      setSelectedProduct(null);
    }
  };

  return (
    <div className={styles.productCard}>
      <img src={product.image} alt={product.name} />
      <h2 className={styles.productName}>{product.name}</h2>
      <p className={styles.productDescription}>{product.shortDescription}</p>
      <p className={styles.productPrice}>Ціна: {product.price} грн</p>

      <div className={styles.productBtns}>
        <button
          onClick={() => {
            setSelectedProduct(product);
            setIsModalOpen(true);
          }}
          className={styles.addToCartBtn}
        >
          Додати в корзину
        </button>

        {isModalOpen && (
          <div className={styles.modal}>
            <span
              className={styles.close}
              onClick={() => setIsModalOpen(false)}
            >
              &times;
            </span>
            <p>Ви впевнені, що хочете додати цей товар в корзину?</p>

            <div className={styles.buttons}>
              <button onClick={() => setIsModalOpen(false)}>Скасувати</button>
              <button
                onClick={() => {
                  handleAddToCart();
                  setIsModalOpen(false);
                }}
              >
                Додати
              </button>
            </div>
          </div>
        )}
        <button
          onClick={() => {
            handleToggleFavorites(product);
          }}
        >
          <FaStar
            className={
              isFavorite
                ? `${styles.myStarIcon} ${styles.isFavorite}`
                : styles.myStarIcon
            }
          />
        </button>
      </div>
    </div>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    shortDescription: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.number,
  }),
  addToCart: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
};

export default ProductCard;
