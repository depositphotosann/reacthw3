import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './FavoritesItem.module.scss';
import { FaStar } from 'react-icons/fa';
import { useImmer } from 'use-immer';

function FavoritesItem({ item = {}, addToFavorites, removeFromFavorites}) {
  const [isFavorite, setIsFavorite] = useImmer(false);



  const toggleFavorite = () => {
    setIsFavorite(!isFavorite);
    if (!isFavorite) {
      addToFavorites(item);
    } else {
      removeFromFavorites(item);
    }
  };

  return (
    <div className={styles.item}>
      <img src={item.image} alt={item.name} />
      <h2 className={styles.productName}>{item.name}</h2>
      <FaStar
        className={isFavorite ? styles.favoriteIconActive : styles.favoriteIconInactive}
        onClick={toggleFavorite}
      />
    </div>
  );
}

FavoritesItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    image: PropTypes.string,
  }),
  addToFavorites: PropTypes.func.isRequired,
  removeFromFavorites: PropTypes.func.isRequired
};

export default FavoritesItem;