import PropTypes from "prop-types";
import ProductCard from "../ProductCard";
import styles from "./ProductsContainer.module.scss";

const ProductsContainer = ({
  products = [],
  addToCart = () => {},
  addToFavorites = () => {},
  favorites,
  addItemToFavorite,
  deleteItemFromFavorite,
}) => {
  return (
    <div className={styles.productsContainer}>
      {products.map((product) => (
        <ProductCard
          key={product.id}
          product={product}
          addToCart={addToCart}
          addToFavorites={addToFavorites}
          favorites={favorites}
          addItemToFavorite={addItemToFavorite}
          deleteItemFromFavorite={deleteItemFromFavorite}
        />
      ))}
    </div>
  );
};

ProductsContainer.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string,
      shortDescription: PropTypes.string,
      image: PropTypes.string,
      price: PropTypes.number,
    })
  ),
  addToCart: PropTypes.func,
  addToFavorites: PropTypes.func,
};

export default ProductsContainer;
