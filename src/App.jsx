import { useEffect, useState } from "react";
import axios from "axios";
import Header from "./components/Header";
import AppRouter from "./AppRouter.jsx";
import { useImmer } from "use-immer";
import {
  readCartFromLS,
  writeCartToLS,
} from "./utils/cartLocalStorageHelpers.js";
import {
  readFavoritesFromLS,
  writeFavoritesToLS,
} from "./utils/favoritesLocalStorageHelpers.js";

function App() {
  const [products, setProducts] = useImmer([]);
  const [cart, setCart] = useImmer([]);
  const [itemToAdd, setItemToAdd] = useImmer(null);
  const [favorites, setFavorites] = useImmer(
    localStorage.getItem("favorites")
      ? JSON.parse(localStorage.getItem("favorites"))
      : []
  );

  const [showModal, setShowModal] = useImmer(false);
  const [itemToDelete, setItemToDelete] = useImmer(null);
  const [itemToDeleteType, setItemToDeleteType] = useImmer(null);

  const cartTotalCount = cart?.reduce((acc, el) => acc + el.count || 0, 0);
  const favoritesTotalCount = favorites.length;

  function deleteItemFromFavorite(id) {
    setFavorites((draft) => {
      const index = draft.findIndex((item) => item.id === id);
      if (index !== -1) draft.splice(index, 1);
    });
  }

  function addItemToFavorite(item) {
    setFavorites((draft) => {
      draft.push(item);
    });
  }

  const addToCart = (product) => {
    setShowModal(true);
    setItemToAdd(product);
    console.log(product);

    setCart((draft) => {
      console.log(draft);
      const cartItem = draft?.find(({ id }) => id === product.id);

      if (!cartItem) {
        draft?.push({ ...product, count: 1 });
      } else {
        cartItem.count++;
      }

      writeCartToLS(draft);
    });
  };

  const decrementCartItem = (itemId) => {
    setCart((draft) => {
      const cartItem = draft.find(({ id }) => id === itemId);

      if (cartItem.count > 1) {
        cartItem.count--;
        writeCartToLS(draft);
      }
    });
  };

  const incrementCartItem = (itemId) => {
    setCart((draft) => {
      const cartItem = draft.find(({ id }) => id === itemId);
      cartItem.count++;
      writeCartToLS(draft);
    });
  };

  const addToFavorites = (product) => {
    console.log(product);

    setFavorites((draft) => {
      console.log(draft);
      const favoritesItem = draft?.find(({ id }) => id === product.id);

      if (!favoritesItem) {
        draft?.push({ ...product, count: 1 });
      } else {
        favoritesItem.count;
      }

      writeFavoritesToLS(draft);
    });
  };

  const decrementFavoritesItem = (itemId) => {
    setFavorites((draft) => {
      const favoritesItem = draft.find(({ id }) => id === itemId);

      if (favoritesItem.count > 1) {
        favoritesItem.count--;
        writeFavoritesToLS(draft);
      }
    });
  };

  const incrementFavoritesItem = (itemId) => {
    setFavorites((draft) => {
      const favoritesItem = draft.find(({ id }) => id === itemId);
      favoritesItem.count++;
      writeFavoritesToLS(draft);
    });
  };

  const handleDelete = () => {
    if (itemToDelete && itemToDeleteType) {
      if (itemToDeleteType === "cart") {
        console.log(handleDelete);
        setCart((draft) => {
          const updatedCartItems = draft.filter(
            (item) => item.id !== itemToDelete.id
          );
          writeCartToLS(updatedCartItems);
          return updatedCartItems;
        });
      } else if (itemToDeleteType === "favorite") {
        setFavorites((draft) => {
          const updatedFavoriteItems = draft.filter(
            (item) => item.id !== itemToDelete.id
          );
          writeFavoritesToLS(updatedFavoriteItems);
          return updatedFavoriteItems;
        });
      }
    }

    setShowModal(false);
    setItemToDelete(null);
    setItemToDeleteType(null);
  };

  const handleOpenModal = (item, type) => {
    setItemToDelete(item);
    setItemToDeleteType(type);
    setShowModal(true);
  };

  const handleOpenCartModal = (item) => {
    setItemToAdd(item);
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  // READ CART STATE FROM LOCAL STORAGE --------------------------------------------------
  useEffect(() => {
    setCart(readCartFromLS());
  }, []);

  // useEffect(() => {
  //   setFavorites(readFavoritesFromLS());
  // }, []);

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
  }, [favorites]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const { data } = await axios.get("./data.json");
        setProducts(data);
      } catch (err) {
        console.log(err);
      }
    };

    fetchProducts();
  }, []);

  return (
    <>
      <Header
        cartTotalCount={cartTotalCount}
        favoritesTotalCount={favoritesTotalCount}
      />

      <main>
        <AppRouter
          products={products}
          addToCart={addToCart}
          cart={cart}
          handleCloseModal={handleCloseModal}
          handleOpenCartModal={handleOpenCartModal}
          decrementCartItem={decrementCartItem}
          incrementCartItem={incrementCartItem}
          addToFavorites={addToFavorites}
          handleDelete={handleDelete}
          favorites={favorites}
          decrementFavoritesItem={decrementFavoritesItem}
          incrementFavoritesItem={incrementFavoritesItem}
          handleOpenModal={handleOpenModal}
          addItemToFavorite={addItemToFavorite}
          deleteItemFromFavorite={deleteItemFromFavorite}
        />
      </main>
    </>
  );
}

export default App;
