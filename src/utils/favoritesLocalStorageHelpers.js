const FAFORITES_LS_KEY = 'favorites';

export const writeFavoritesToLS = (favorites) => {
    localStorage.setItem(FAFORITES_LS_KEY, JSON.stringify(favorites));
}

export const readFavoritesFromLS = () => {
    const favorites = localStorage.getItem(FAFORITES_LS_KEY);

    if (favorites) {
        return JSON.parse(favorites);
    } else {
        return [];
    }
}
