const CART_LS_KEY = 'cart';

export const writeCartToLS = (cart) => {
    localStorage.setItem(CART_LS_KEY, JSON.stringify(cart));
}

export const readCartFromLS = () => {
    const cart = localStorage.getItem(CART_LS_KEY);

    if (cart) {
        return JSON.parse(cart);
    } else {
        return [];
    }
}
