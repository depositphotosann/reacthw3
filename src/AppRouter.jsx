import { Routes, Route, Navigate } from "react-router-dom";
import HomeRoute from "./routes/HomeRoute";
import CartRoute from "./routes/CartRoute";
import FavoritesRoute from "./routes/FavoritesRoute";
import PropTypes from "prop-types";

const AppRouter = ({
  products = [],
  addToCart = () => {},
  cart = [],
  decrementCartItem = () => {},
  incrementCartItem = () => {},
  handleDelete = () => {},

  addToFavorites = () => {},
  favorites = [],
  decrementFavoritesItem = () => {},
  incrementFavoritesItem = () => {},
  handleOpenModal,
  addItemToFavorite,
  deleteItemFromFavorite,
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomeRoute
            addToCart={addToCart}
            addToFavorites={addToFavorites}
            products={products}
            favorites={favorites}
            addItemToFavorite={addItemToFavorite}
            deleteItemFromFavorite={deleteItemFromFavorite}
          />
        }
      />
      <Route
        path="/cart"
        element={
          <CartRoute
            cart={cart}
            decrementCartItem={decrementCartItem}
            incrementCartItem={incrementCartItem}
            handleDelete={handleDelete}
            handleOpenModal={handleOpenModal}
          />
        }
      />
      <Route
        path="/favorites"
        element={
          <FavoritesRoute
            favorites={favorites}
            decrementFavoritesItem={decrementFavoritesItem}
            incrementFavoritesItem={incrementFavoritesItem}
            handleOpenModal={handleOpenModal}
          />
        }
      />
    </Routes>
  );
};

AppRouter.propTypes = {
  products: PropTypes.array,
  cart: PropTypes.array,
  addToCart: PropTypes.func,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  favorites: PropTypes.array,
  addToFavorites: PropTypes.func,
  decrementFavoritesItem: PropTypes.func,
  incrementFavoritesItem: PropTypes.func,
  handleOpenModal: PropTypes.func.isRequired,
};

export default AppRouter;
