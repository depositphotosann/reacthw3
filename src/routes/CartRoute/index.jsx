import PropTypes from 'prop-types';
import CartItem from '../../components/CartItem';
import styles from './CartRoute.module.scss';

const CartRoute = ({ 
  cart = [],
  decrementCartItem = () => {},
  incrementCartItem = () => {},
  handleDelete = () => {},
  handleCloseModal,
  handleOpenModal
 }) => {

  const totalPrice = cart.reduce((acc, el) => acc + el.price * el.count, 0)

  return (
    <>
      <h1>Корзина</h1>

      <div>
        {cart.map(item => <CartItem 
        key={item.id} 
        item={item}
        decrementCartItem={decrementCartItem}
        incrementCartItem={incrementCartItem}
        handleDelete = {handleDelete}
        handleOpenModal={handleOpenModal}
        handleCloseModal={handleCloseModal}
        />)}
      </div>

      <h2 className={styles.totalPrice}> ЗАГАЛЬНА СУМА: { totalPrice } грн</h2> 
    </>
  )
}

CartRoute.propTypes = {
  cart: PropTypes.array,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func
};

export default CartRoute;