import PropTypes from "prop-types";
import ProductsContainer from "../../components/ProductsContainer";

const HomeRoute = ({
  products = [],
  addToCart = () => {},
  addToFavorites = () => {},
  favorites = [],
  addItemToFavorite,
  deleteItemFromFavorite,
}) => {
  return (
    <>
      <h1>Нумізматична продукція</h1>
      <ProductsContainer
        products={products}
        addToCart={addToCart}
        addToFavorites={addToFavorites}
        favorites={favorites}
        addItemToFavorite={addItemToFavorite}
        deleteItemFromFavorite={deleteItemFromFavorite}
      />
    </>
  );
};

HomeRoute.propTypes = {
  products: PropTypes.array,
  addToCart: PropTypes.func,
  addToFavorites: PropTypes.func,
};

export default HomeRoute;
