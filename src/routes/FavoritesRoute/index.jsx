import PropTypes from "prop-types";
import FavoritesItem from "../../components/FavoritesItem";
import styles from "./FavoritesRoute.module.scss";
import { useImmer } from "use-immer";

const FavoritesRoute = ({
  favorites = [],
  decrementFavoritesItem = () => {},
  incrementFavoritesItem = () => {},
  handleOpenModal,
  removeFromFavorites,
}) => {
  console.log(favorites);

  const totalCount = favorites.length;

  const [favoriteItems, setFavoriteItems] = useImmer(favorites);

  const removeFromFavoritesHandler = (itemToRemove) => {
    const updatedFavorites = favoriteItems.filter(
      (item) => item.id !== itemToRemove.id
    );
    setFavoriteItems(updatedFavorites);
  };

  return (
    <>
      <h1>ОБРАНЕ</h1>

      <div>
        {favorites.map((item) => (
          <FavoritesItem
            key={item.id}
            item={item}
            decrementFavoritesItem={decrementFavoritesItem}
            incrementFavoritesItem={incrementFavoritesItem}
            handleOpenModal={handleOpenModal}
            removeFromFavorites={removeFromFavoritesHandler}
          />
        ))}
      </div>

      <h2 className={styles.totalCount}> ЗАГАЛЬНА КІЛЬКІСТЬ: {totalCount}</h2>
    </>
  );
};

FavoritesRoute.propTypes = {
  favorites: PropTypes.array,
  decrementFavoritesItem: PropTypes.func,
  incrementFavoritesItem: PropTypes.func,
  handleOpenModal: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  removeFromFavorites: PropTypes.func.isRequired,
};

export default FavoritesRoute;
